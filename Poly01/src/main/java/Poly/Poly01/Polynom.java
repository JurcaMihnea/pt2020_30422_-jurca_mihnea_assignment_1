package Poly.Poly01;

import java.util.ArrayList;
import java.util.Collections;

public class Polynom {
	
	private ArrayList<Monom> poly;
	
	
	
	public ArrayList<Monom> getPoly() {
		return poly;
	}

	public void setPoly(ArrayList<Monom> poly) {
		this.poly = poly;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((poly == null) ? 0 : poly.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Polynom other = (Polynom) obj;
		if (poly == null) {
			if (other.poly != null)
				return false;
		} else if (!poly.equals(other.poly))
			return false;
		return true;
	}

	
	public Polynom(ArrayList<Monom> poly){
		
		for (int i=0; i< poly.size(); i++){
		    for(int j=0; j< poly.size()-1; j++){
		    	if (poly.get(i).getDegree() > poly.get(j).getDegree()){
		    		Collections.swap(poly, i, j);
		    	}
		    } 
		}
		this.poly = poly;
	}
	
	public Polynom(){	
		}
	
	public int maxDegree(Polynom p1, Polynom p2){
		return Math.max(p1.poly.size(), p2.poly.size());
	}
	
	public boolean maxPolynom(Polynom p1, Polynom p2, int it1, int it2){// gradul p1 > gradul p2 true daca mai mic sau egal atunci false
		if( p1.poly.get(it1).getDegree() > p2.poly.get(it2).getDegree()){
			return true;
		}else return false;
	}
	
	public Polynom add(Polynom pollToAdd){
		
		
		ArrayList<Monom> aux = new ArrayList<Monom>();
		ArrayList<Integer> deg = new ArrayList<Integer>();	
		
		for(int i = 0; i < this.poly.size(); i++){
			for(int j = 0; j < pollToAdd.poly.size(); j++){
				if(this.poly.get(i).getDegree() == pollToAdd.poly.get(j).getDegree()){
					Monom e = new Monom(this.poly.get(i).getConstant()+ pollToAdd.poly.get(j).getConstant(), pollToAdd.poly.get(j).getDegree());
					aux.add(e);
					deg.add(pollToAdd.poly.get(j).getDegree());
				}
			}
		}	
		for(int i = 0; i < this.poly.size(); i++){
			if(!deg.contains(this.poly.get(i).getDegree())) {
				aux.add(this.poly.get(i));
			}
		}
		
		for(int i = 0; i < pollToAdd.poly.size(); i++){
			if(!deg.contains(pollToAdd.poly.get(i).getDegree())) {
				aux.add(pollToAdd.poly.get(i));
			}
		}
		
		for(int i = 0; i < aux.size(); i++){
			if(aux.get(i).getConstant() == 0) {
				aux.remove(i);
			}
		}
		
		Polynom rez = new Polynom(aux);
		return rez;
		
	}

	public Polynom sub(Polynom pollToSub){
		
		ArrayList<Monom> aux = new ArrayList<Monom>();
		ArrayList<Integer> deg = new ArrayList<Integer>();
		
		for(int i = 0; i < this.poly.size(); i++){
			for(int j = 0; j < pollToSub.poly.size(); j++){
				if(this.poly.get(i).getDegree() == pollToSub.poly.get(j).getDegree()){
					Monom e = new Monom(this.poly.get(i).getConstant() - pollToSub.poly.get(j).getConstant(), pollToSub.poly.get(j).getDegree());
					aux.add(e);
					deg.add(pollToSub.poly.get(j).getDegree());
				}
			}
		}	
		for(int i = 0; i < this.poly.size(); i++){
			if(!deg.contains(this.poly.get(i).getDegree())) {
				aux.add(this.poly.get(i));
			}
		}
		
		for(int i = 0; i < pollToSub.poly.size(); i++){
			if(!deg.contains(pollToSub.poly.get(i).getDegree())) {
				Monom a = new Monom(-pollToSub.poly.get(i).getConstant(), pollToSub.poly.get(i).getDegree());
				aux.add(a);
			}
		}
		
		for(int i = 0; i < aux.size(); i++){
			if(aux.get(i).getConstant() == 0) {
				aux.remove(i);
			}
		}
		
		Polynom rez = new Polynom(aux);
		return rez;
					
		}
	
	public Polynom deriv(){
		
		
		ArrayList<Monom> aux1 = new ArrayList<Monom>();
		
		
		for(int i=0; i < this.poly.size(); i++){		
			Monom copy = new Monom(this.poly.get(i).getConstant() * this.poly.get(i).getDegree(), this.poly.get(i).getDegree() - 1);
			aux1.add(copy);
		}
			
		Polynom rez = new Polynom(aux1);
		return rez;
		
	}
	
	
	public Polynom integration(){
			
			
			ArrayList<Monom> aux1 = new ArrayList<Monom>();
			
			
			for(int i=0; i < this.poly.size(); i++){		
				Monom copy = new Monom(this.poly.get(i).getConstant(), this.poly.get(i).getDegree() + 1);
				aux1.add(copy);
			}
				
			Polynom rez = new Polynom(aux1);
			return rez;
			
		}
	

	public Polynom mul(Polynom pollToMul){
		ArrayList<Monom> aux = new ArrayList<Monom>();
		
		for (int i = 0; i < this.poly.size(); i++ ){
			for (int j = 0; j < pollToMul.poly.size(); j++ ){
					Monom loc = new Monom(this.poly.get(i).getConstant() * pollToMul.poly.get(j).getConstant(), this.poly.get(i).getDegree() + pollToMul.poly.get(j).getDegree());
					aux.add(loc);
			}
		}
		Polynom rez = new Polynom(aux);
		int i = 0;
		while(i < rez.poly.size()-1){
			while(rez.poly.get(i).getDegree() == rez.poly.get(i+1).getDegree()){
				Monom a = rez.poly.get(i).monomAdd(rez.poly.get(i+1));
				rez.poly.set(i, a);
				rez.poly.remove(i+1);
			}
			i++;
		}
		return rez;
	}
	
	public int MaxDegree(Polynom p){
		
		return p.poly.get(0).getDegree();
		
	}

	public char signNum(int x){
		if (x >= 0){
			return '+';
		}else{
			return '-';
		}
	}
	
	public String div(Polynom toDiv){
		String rez;
		Polynom n = this;
		Polynom d = toDiv;
		ArrayList<Monom> monomArray = new ArrayList<Monom>();
		Monom zero = new Monom(0, 0);
		monomArray.add(zero);
		Polynom q = new Polynom(monomArray);
		Polynom r = this;
		
		while ((r.poly.size() != 0) && (r.poly.get(0).getDegree() >= d.poly.get(0).getDegree())){
				 double x = r.poly.get(0).getConstant()/d.poly.get(0).getConstant();
				 int y = (int) x;
		         Monom aux = new Monom(y, r.poly.get(0).getDegree()-d.poly.get(0).getDegree());	         
		         ArrayList<Monom> polAux = new ArrayList<Monom>();
		         polAux.add(aux);
		         Polynom t = new Polynom(polAux);
		         
		         q = q.add(t);
		         Polynom auxr = t.mul(d);
		         r = r.sub(auxr);
		     
		}
		rez = "Catul este : ";
		for(int i =0; i < q.poly.size(); i++)	{	
			rez = rez + signNum(q.poly.get(i).getConstant()) + q.poly.get(i).getConstant() + "*X^"  + q.poly.get(i).getDegree() ;
		}
		rez = rez + " Restul este: ";
		for(int i =0; i < r.poly.size(); i++)	{	
			rez = rez + signNum(r.poly.get(i).getConstant()) + r.poly.get(i).getConstant() + "*X^"  + r.poly.get(i).getDegree();
		}
		
		return rez;
	}
	
	public String setResult() { 
		String aux = "";
				
		for(int i =0; i < this.poly.size(); i++)	{	
			aux = aux + signNum(this.poly.get(i).getConstant()) + this.poly.get(i).getConstant() + "*X^"  + this.poly.get(i).getDegree();
		}
		
		return aux;
		
	}
}

