package Poly.Poly01;

public class MVCCalculato {
	
	public static void main(String[] args){
		View theView = new View();
		Polynom poly = new Polynom();
		@SuppressWarnings("unused")
		Controller controller = new Controller(theView, poly);
		
		theView.setVisible(true);
	}
	
}
