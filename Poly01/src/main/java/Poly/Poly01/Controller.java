package Poly.Poly01;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controller {
	private View theView;
	private Polynom thePolynom;
	
	public Controller(View theView, Polynom thePolynom){
		this.theView = theView;
		this.thePolynom = thePolynom;
		
		this.theView.addCalculationListener(new AdditionCalculateListener());
		this.theView.subCalculationListener(new SubstractionCalculateListener());
		this.theView.derivCalculationListener(new DerivativeCalculateListener());
		this.theView.integCalculationListener(new IntegralCalculateListener());
		this.theView.mulCalculationListener(new MulCalculateListener());
		this.theView.divCalculationListener(new DivCalculateListener());
		
	}
	
		class AdditionCalculateListener implements ActionListener{
	
			public void actionPerformed(ActionEvent e) {
				Polynom p1 = new Polynom();
				Polynom p2 = new Polynom();
			
			p1 = theView.getPolynom(theView.poly1);
			p2 = theView.getPolynom(theView.poly2);
			
			Polynom rez = new Polynom();
			rez = p1.add(p2);
			
			theView.setResult(rez);
			}
		}
		
		class SubstractionCalculateListener implements ActionListener{
			
			public void actionPerformed(ActionEvent e) {
				Polynom p1 = new Polynom();
				Polynom p2 = new Polynom();
			
			p1 = theView.getPolynom(theView.poly1);
			p2 = theView.getPolynom(theView.poly2);
			
			Polynom rez = new Polynom();
			rez = p1.sub(p2);
			
			theView.setResult(rez);
			}
		}
		
		class DerivativeCalculateListener implements ActionListener{
			
			public void actionPerformed(ActionEvent e) {
				Polynom p1 = new Polynom();
				
			
			p1 = theView.getPolynom(theView.poly1);
			
			Polynom rez = new Polynom();
			rez = p1.deriv();
			
			theView.setResult(rez);
			}
		}
		
		class IntegralCalculateListener implements ActionListener{
					
					public void actionPerformed(ActionEvent e) {
						Polynom p1 = new Polynom();
					
					p1 = theView.getPolynom(theView.poly1);
					
					Polynom rez = new Polynom();
					rez = p1.integration();
					
					theView.setResultForIntegral(rez);
					}
				}
		
		class MulCalculateListener implements ActionListener{
			
			public void actionPerformed(ActionEvent e) {
				Polynom p1 = new Polynom();
				Polynom p2 = new Polynom();
				
			p1 = theView.getPolynom(theView.poly1);
			p2 = theView.getPolynom(theView.poly2);
			
			Polynom rez = new Polynom();
			rez = p1.mul(p2);
			
			theView.setResult(rez);
			}
		}
		
class DivCalculateListener implements ActionListener{
			
			public void actionPerformed(ActionEvent e) {
				Polynom p1 = new Polynom();
				Polynom p2 = new Polynom();
				
			p1 = theView.getPolynom(theView.poly1);
			p2 = theView.getPolynom(theView.poly2);
			theView.setResult(p1.div(p2));
			}
		}
}
