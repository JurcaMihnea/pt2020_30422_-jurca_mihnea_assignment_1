package Poly.Poly01;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class JunitPoly {

	@Test
	public void testMonomAddition() {
		Monom m1 = new Monom(6, 2);
		Monom m2 = new Monom(3, 2);
		Monom m3 = m1.monomAdd(m2);
		Monom rez = new Monom(9, 2);
		assertEquals(rez, m3);
	}

	@Test
	public void testMonomSubtraction() {
		Monom m1 = new Monom(6, 2);
		Monom m2 = new Monom(3, 2);
		Monom m3 = m1.monomSub(m2);
		Monom rez = new Monom(3, 2);
		assertEquals(rez, m3);
	}

	@Test
	public void testMonomMultiplication() {
		Monom m1 = new Monom(6, 2);
		Monom m2 = new Monom(3, 2);
		Monom m3 = m1.monomMul(m2);
		Monom rez = new Monom(18, 4);
		assertEquals(rez, m3);
	}

	@Test
	public void testMonomDifferentiation() {
		Monom m1 = new Monom(6, 2);
		Monom m2 = m1.monomDeriv();
		Monom rez = new Monom(12, 1);
		assertEquals(rez, m2);
	}
	
	@Test
	public void testMonomIntergration() {
		Monom m1 = new Monom(6, 2);
		Monom m2 = m1.monomInt();
		Monom rez = new Monom(6, 3);
		assertEquals(rez, m2);
	}

	@Test
	public void testPolynomAddition() {
		ArrayList<Monom> pol1 = new ArrayList<Monom>();
		ArrayList<Monom> pol2 = new ArrayList<Monom>();
		Monom m1 = new Monom(3,2);
		Monom m2 = new Monom(4,1);
		Monom m3 = new Monom(1,1);
		Monom m4 = new Monom(-4,0);
		pol1.add(m1);
		pol1.add(m2);
		pol2.add(m3);
		pol2.add(m4);
		Polynom p1 = new Polynom(pol1);
		Polynom p2 = new Polynom(pol2);
		Polynom rez = p1.add(p2);
		String output = rez.setResult();
		assertEquals("+3*X^2+5*X^1--4*X^0", output);
	}
	
	public void testPolynomSubtraction() {
		ArrayList<Monom> pol1 = new ArrayList<Monom>();
		ArrayList<Monom> pol2 = new ArrayList<Monom>();
		Monom m1 = new Monom(3,2);
		Monom m2 = new Monom(4,1);
		Monom m3 = new Monom(1,1);
		Monom m4 = new Monom(-4,0);
		pol1.add(m1);
		pol1.add(m2);
		pol2.add(m3);
		pol2.add(m4);
		Polynom p1 = new Polynom(pol1);
		Polynom p2 = new Polynom(pol2);
		Polynom rez = p1.sub(p2);
		String output = rez.setResult();
		assertEquals("+3*x^2+3*x^1+4*x^1", output);
	}
	
	@Test
	public void testPolynomMultiplication() {
		ArrayList<Monom> pol1 = new ArrayList<Monom>();
		ArrayList<Monom> pol2 = new ArrayList<Monom>();
		Monom m1 = new Monom(3,2);
		Monom m2 = new Monom(4,1);
		Monom m3 = new Monom(1,1);
		Monom m4 = new Monom(-4,0);
		pol1.add(m1);
		pol1.add(m2);
		pol2.add(m3);
		pol2.add(m4);
		Polynom p1 = new Polynom(pol1);
		Polynom p2 = new Polynom(pol2);
		Polynom rez = p1.mul(p2);
		String output = rez.setResult();
		assertEquals("+3*X^3--8*X^2--16*X^1", output);
	}
	
	@Test
	public void testPolynomDivision() {
		ArrayList<Monom> pol1 = new ArrayList<Monom>();
		ArrayList<Monom> pol2 = new ArrayList<Monom>();
		Monom m1 = new Monom(1,3);
		Monom m2 = new Monom(-2,2);
		Monom m3 = new Monom(-4,0);
		Monom m4 = new Monom(1,1);
		Monom m5 = new Monom(-3,0);
		pol1.add(m1);
		pol1.add(m2);
		pol1.add(m3);
		pol2.add(m4);
		pol2.add(m5);
		Polynom p1 = new Polynom(pol1);
		Polynom p2 = new Polynom(pol2);
		String rez = p1.div(p2);
		assertEquals("Catul este : +1*X^2+1*X^1+3*X^0 Restul este: +5*X^0", rez);
	}
	
	@Test
	public void testPolynomDifferentiation() {
		ArrayList<Monom> pol1 = new ArrayList<Monom>();
		Monom m1 = new Monom(3,2);
		Monom m2 = new Monom(4,1);
		pol1.add(m1);
		pol1.add(m2);
		Polynom p1 = new Polynom(pol1);
		Polynom rez = p1.deriv();
		String output = rez.setResult();
		assertEquals("+6*X^1+4*X^0", output);
	}
	
	@Test
	public void testPolynomIntegration() {
		ArrayList<Monom> pol1 = new ArrayList<Monom>();
		Monom m1 = new Monom(3,2);
		Monom m2 = new Monom(4,1);
		pol1.add(m1);
		pol1.add(m2);
		Polynom p1 = new Polynom(pol1);
		Polynom rez = p1.integration();
		String output = rez.setResult();
		assertEquals("+3*X^3+4*X^2", output);
	}	

}
